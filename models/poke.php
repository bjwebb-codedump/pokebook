<?php
class Poke extends AppModel
{
 var $name = 'Poke';
 var $belongsTo = array(
        'UserTo' => array(
            'className'     => 'User',
            'foreignKey'    => 'to_user_id'
        ),
        'UserFrom' => array(
            'className'     => 'User',
            'foreignKey'    => 'from_user_id'
        )
    );
 var $order = "Poke.created DESC";
}
?>