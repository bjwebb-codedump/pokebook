<?php
class User extends AppModel {
    var $name = 'User';
    var $hasMany = array(
        'PokeTo' => array(
            'className'     => 'Poke',
            'foreignKey'    => 'to_user_id',
            'order'    => 'PokeTo.created DESC'
        ),
        'PokeFrom' => array(
            'className'     => 'Poke',
            'foreignKey'    => 'from_user_id',
            'order'    => 'PokeFrom.created DESC'
        )
    );
    var $recursive = 2;
    var $order = "User.created DESC";
 
    var $validate = array(
    'username' => array (
           "notEmpty" => array('rule'=>"notEmpty", 'message'=>"Must not be empty!"),
           "alphaNumeric" => array('rule'=>"alphaNumeric", 'message'=>"Alphanumeric characters only."),
           "isUnique" => array('rule'=>"isUnique", 'message'=>"Sorry, this username already exists.")
       ),
     'password1' => array (
           "notEmpty" => array('rule'=>"notEmpty", 'message'=>"Must not be empty!"),
       ),
     'password2' => array (
           "identicalFieldValues" => array('rule'=>array("identicalFieldValues", "password1"), 'message'=>"Passwords do not match!")
     )
    );
 
    function identicalFieldValues( $field=array(), $compare_field=null ) 
    {
        foreach( $field as $key => $value ){
            $v1 = $value;
            $v2 = $this->data[$this->name][ $compare_field ];                 
            if($v1 !== $v2) {
                return FALSE;
            } else {
                continue;
            }
        }
        return TRUE;
    }
}
?>