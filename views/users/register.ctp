<h2>Register</h2>
<?php echo $form->create('User', array('action' => 'register')); ?>

<table class="login">
    <tr>
        <td class="labelrow"><label for="username">Username: </label></td><td><?php echo $form->input('User.username', array('label'=>false))?></td>
    </tr>
    <tr>
        <td class="labelrow"><label for="email">Email: </label></td><td><?php echo $form->input('User.email', array('label'=>false))?></td>
    </tr>
    <tr>
        <td class="labelrow"><label for="password1">Password: </label></td><td><?php echo $form->input('User.password1', array('label'=>false, 'type'=>"password"))?></td> 
    </tr>
    <tr>
        <td class="labelrow"><label for="password2">Repeat Password: </label></td><td><?php echo $form->input('User.password2', array('label'=>false, 'type'=>"password"))?></td> 
    </tr>
    <tr>
        <td></td><td><?php echo $form->submit('Register') ?></td>
    </tr>
</table>

<?php echo $form->end(); ?>
