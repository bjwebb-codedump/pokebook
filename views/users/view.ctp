<?php
$javascript->link('prototype', false);
$javascript->link('scriptaculous', false);

#print_r($paginator->params['paging']['Poke']['count']);

function pokes(&$pokes, &$html) {
    foreach ($pokes as $poke) {
        $u1 = $poke['UserFrom']['username'];
        $u2 = $poke['UserTo']['username'];
        echo '<div class="poke">';
        echo '<div>'.$html->link($u1, "/$u1").' poked '.$html->link($u2, "/$u2").'</div>';
        echo '<div class="date">'.$html->link(date("H:i M j Y", strtotime($poke['Poke']['created'])), "/pokes/view/".$poke['Poke']['id']).'</div>';
        echo '</div>';
    }
}

if ($user) {
    #print_r($user);
    $u = $user['username'];
    
    if ($isAjax) {
        if ($_REQUEST['dir'] == 'from')  pokes($PokeFrom, $html, $u);
        elseif ($_REQUEST['dir'] == 'to')  pokes($PokeTo, $html, $u);
    }
    else {
        echo '<div class="column">'."\n";
        echo "<h2>$u&rsquo;s pokes"."</h2>";
        echo "<ul id=\"PokeFrom\">\n";
        pokes($PokeFrom, $html);
        echo "</ul>\n";
        echo "</div>\n";
    
        echo '<div class="column">'."\n";
        echo "<h2>poking $u"."</h2>";
        echo "<ul id=\"PokeTo\">\n";
        pokes($PokeTo, $html);
        echo "</ul>\n";
        echo "</div>\n";
    
    ?>
        <div class="column">
         <h2>about <?=$u?></h2>
         <img class="user" src="<?=$html->url('/img/alpaca.jpg')?>" alt="bjwebb" />
         <?/*<ul>
          <li><strong>age:</strong> <?=$user['age']?></li>
          <li><strong>another:</strong> secondary</li>
    
          <li><strong>test:</strong> no test</li>
         </ul>*/?>
         <ul>
          <li><strong>pokes sent:</strong> <?=$PokeFromCount?></li>
          <li><strong>poked recieved:</strong> <?=$PokeToCount?></li>
    
         </ul>
        </div>
        
        <div class="column">
         <? if ($me = $session->read('Auth.User')) {
                if ($me['id'] != $user['id']) { ?>
         <h2>actions</h2>
         <form action="<?=$html->url('/users/poke/'.$u)?>" method="get">
          <input type="submit" value="poke" class="poke" />
         </form>
         <? } } else { ?>
         <p><?=$u?> is using <?=$html->link("pokebook","/")?>, a cool and trendy new way to nudge people. <?=$html->link("Sign up", "/users/register")?>, or you're not cool!</p>
         <? } ?>
        </div>
        
        <br clear="all" />
        
        <div class="doublecolumn">
        <?php
        $paginator->options['url'] = $u;
        if ($paginator->hasPrev())
            echo '<div style="float:left">' . $paginator->prev("< Previous ") . "</div>";
        if ($paginator->hasNext())
            echo '<div style="float:right">' . $paginator->next(" Next >") . "</div>";
        ?>
        </div>
<?php

        if ($paginator->current() == 1) {
            echo $ajax->remoteTimer(
                array(
                'url' => "/users/view/$u?dir=from",
                'update' => 'PokeFrom'#, 'complete' => 'alert( "request completed" )',
                #'position' => 'bottom', 'frequency' => 5
                )
            );
            echo $ajax->remoteTimer(
                array(
                'url' => "/users/view/$u?dir=to",
                'update' => 'PokeTo'
                )
            );
        }
    }

}
else {
    echo "Sorry, that user does not exist.";
}
?>