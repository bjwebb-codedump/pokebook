<h2>Change Password</h2>
<?php echo $form->create('User', array('action' => 'changepassword')); ?>

<table class="login">
    <tr>
        <td class="labelrow"><label for="passwordc">Current Password: </label></td><td><?php echo $form->input('User.passwordc', array('label'=>false, 'type'=>"password"))?></td> 
    </tr>
    <tr>
        <td class="labelrow"><label for="password1">New Password: </label></td><td><?php echo $form->input('User.password1', array('label'=>false, 'type'=>"password"))?></td> 
    </tr>
    <tr>
        <td class="labelrow"><label for="password2">Repeat New Password: </label></td><td><?php echo $form->input('User.password2', array('label'=>false, 'type'=>"password"))?></td> 
    </tr>
    <tr>
        <td></td><td><?php echo $form->submit('Change Password') ?></td>
    </tr>
</table>

<?php echo $form->end(); ?>