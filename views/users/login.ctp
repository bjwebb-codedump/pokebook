<h2>Log In</h2>
<?php
if ($session->read('Auth.User')) {
    echo "You are already logged in! ";
    echo $html->link("Logout?", "/users/logout");
}
else {
    echo "Don't have an account yet? ";
    echo $html->link("Register.", "/users/register");
    echo "<br/><br/>";
    $session->flash();
    $session->flash('auth');
    
echo $form->create('User', array('action' => 'login'));
?>

<table class="login">
    <tr>
        <td class="labelrow"><label for="username">Username: </label></td><td><?php echo $form->input('User.username', array('label'=>false))?></td>
    </tr>
    <tr>
        <td class="labelrow"><label for="password1">Password: </label></td><td><?php echo $form->input('User.password', array('label'=>false))?></td><td><?php echo $form->submit('Login') ?></td>
    </tr>
</table>

<?php echo $form->end();

}
?>