<h2>Settings</h2>
<?php echo $html->link("Change password.", '/users/changepassword/'); ?>

<?php echo $form->create('User', array('action' => 'settings')); ?>

<table class="login">
    <tr>
        <td class="labelrow"><label for="username">Username: </label></td><td><?php echo $form->input('User.username', array('label'=>false))?></td>
    </tr>
    <tr>
        <td class="labelrow"><label for="email">Email: </label></td><td><?php echo $form->input('User.email', array('label'=>false))?></td>
    </tr>
    <tr>
        <td></td><td><?php echo $form->submit('Update') ?></td>
    </tr>
</table>

<?php echo $form->end(); ?>