<?php
    function clean_pokes(&$pokes) {
        foreach ($pokes as &$poke) {
            unset($poke['UserTo']['password']);
            unset($poke['UserTo']['email']);
            unset($poke['UserFrom']['password']);
            unset($poke['UserFrom']['email']);
        }
    }
    
    unset($user['password']);
    unset($user['email']);
    clean_pokes($PokeTo);
    clean_pokes($PokeFrom);
    echo $javascript->object(array(
        "User" => $user,
        "PokeTo" => $PokeTo,
        "PokeFrom" => $PokeFrom
    ));
?>