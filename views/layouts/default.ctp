<?php $user = $session->read('Auth.User') ?>
<html>
 <head>
  <title>Pokebook 2.0<?/* :: */?></title>
  <?= $html->css('main', 'stylesheet', array("media"=>"all")); ?>
  <?php echo $scripts_for_layout ?>
 </head>
 <body>
  <div id="banner">
   <div class="wrap">
    <a href="<?=$html->url($user?'/'.$user['username']:'/')?>"><img src="<?=$html->url('/img/logo.png')?>" alt="Pokebook beta" /></a>

   </div>
  </div>
  <div id="navi">
   <div class="wrap">
    <div class="left">
     <?= $html->link("home", "/") ?>
     <?= $html->link("public", "/pokes") ?>
     <?= $html->link("pokers", "/users") ?>
     <?= $html->link("twitter", "http://twitter.com/pokebook") ?> 

    </div>
    <div class="right">
     <? if ($user) { ?>
     <?=$html->link($user["username"], "/".$user["username"])?>
     <?=$html->link("settings", "/users/settings")?>
     <?=$html->link("logout", "/users/logout")?>
     <? } else { ?>
     <?=$html->link("register", "/users/register")?>
     <?=$html->link("login", "/users/login")?>
     <? } ?>
    </div>
   </div>
  </div>
  <div class="wrap">

   <div id="main">
    <?php echo $content_for_layout ?>

    <div class="clear"></div>
   </div>
    <div id="foot"><a href="http://pokebook.co.uk">Pokebook</a> is a product of <a href="http://twitter.com/pokebook">Pokebook Inc</a>. Alpaca photo by <a href="http://www.flickr.com/photos/phoosh/130415628/">Phoosh</a> (cc).</div>
  </div>

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//stats.tdobson.net/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 6]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//stats.tdobson.net/piwik.php?idsite=6" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

 </body>

</html>
