<div class="doublecolumn">
   <img src="http://pokebook.co.uk/img/alpaca.jpg" />
</div>
<div class="doublecolumn">
<h2>Welcome to Pokebook!</h2>
<!--<p>Yo! Pokebook is the new hip service for getting your buddies' attention, you may know us from our innovative <a href="http://freedomdreams.co.uk/files/pokebook_ad.ogv">ad campaign</a>. <?php echo $html->link("Log in","/users/login") ?>, or if you're not yet chilling with us, why not <?php echo $html->link("sign up","/users/register") ?> for an account?</p>-->
    
<h3>Like to stay in touch with you friends? You'll love Pokebook!</h3>
<p><em>Pokebook is a new, innovative service that allows you to get your friends attention.</em></p>
<h4><?=$html->link("Register now!","/users/register")?> It only takes a few seconds!</h4>

<!--<p style="margin-top: 3em">Check out our latest marketing video:<br/></p>-->

<p style="margin-top:3em">Feeling bored? Check out our <a href="http://twitter.com/pokebook">twitter</a> for all the latest insider rumours or friend us on <a href="https://facebook.com/pokebook.co.uk">Facebook</a>.</p>

</div>
