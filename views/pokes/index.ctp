<h2>All Pokes</h2>
<?php
function pokes(&$pokes, &$html) {
    foreach ($pokes as $poke) {
        $u1 = $poke['UserFrom']['username'];
        $u2 = $poke['UserTo']['username'];
        $poke = $poke['Poke'];
        echo '<div class="poke">';
        echo '<div>'.$html->link($u1, "/$u1").' poked '.$html->link($u2, "/$u2").'</div>';
        echo '<div class="date">'.$html->link(date("H:i M j Y", strtotime($poke['created'])), "/pokes/view/".$poke['id']).'</div>';
        echo '</div>';
    }
}
$cols = array_chunk($pokes,ceil(count($pokes)/4));
for ($i=0; $i<4; $i++) {
    echo '<div class="column">'."\n";
    pokes($cols[$i], $html);
    echo "</div>\n";
}
?>

<br clear="all" />
<?php #echo $paginator->numbers(); ?>
<?php
        if ($paginator->hasPrev())
            echo '<div style="float:left">' . $paginator->prev("< Previous ") . "</div>";
        if ($paginator->hasNext())
            echo '<div style="float:right">' . $paginator->next(" Next >") . "</div>";
?>
<?php #echo $paginator->counter(); ?>