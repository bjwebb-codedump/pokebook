<?php
class UsersController extends AppController
{
    var $name = 'Users';
    var $components = array('Auth', 'RequestHandler');
    var $uses = array('User', 'Poke');
    var $helpers = array('Form', 'Html', 'Javascript', 'Ajax');
    var $paginate = array(
        'limit'=>10,
        'order' => array(
            'Poke.created' => 'desc'
        )
    );
    
    function index() {
        $this->User->recursive = 0;
        $users = $this->User->findAll();
        $this->set('users', $users);
    }
    
    function login() {
    }
    
    function logout() {
        $this->redirect($this->Auth->logout());
    }
    
    function register() {
        if (!empty($this->data['User'])) {
            $data = $this->data;
            $data['User']["password"] = $data['User']["password1"];
            $data = $this->Auth->hashPasswords($data);
            if ($this->User->save($data['User'])) {
                $this->flash('You have succesfully registered, please sign in.','/users/login');
            }
        }
    }
    
    function settings() {
        $this->User->id = $this->Auth->user('id');
        if (empty($this->data)) {
		$this->data = $this->User->read();
	} else {
            $data = $this->data;
            if ($this->User->save($data['User'])) {
                $this->flash('Settings updated successfully.','/users/settings');
            }
        }
    }

    function changepassword() {
        $this->User->id = $this->Auth->user('id');
        if (empty($this->data)) {
		$this->data = $this->User->read();
	} else {
            $data = $this->data;
            $odata = $this->User->read();
            if ($this->Auth->password($data['User']['passwordc']) == $odata['User']['password']) {
                $data['User']["password"] = $this->Auth->password($data['User']["password1"]);
                if ($this->User->save($data['User'])) {
                    $this->flash('Password updated successfully.','/users/changepassword');
                }
            }
            else {
                $this->flash('Current password incorrect!.','/users/changepassword');
            }
        }
    }
    
    function view() {
        $p = $this->params['pass'];
        if (count($p)) {
            $user = $this->User->find('first', array('conditions' => array('User.username' => $p[0])));
            $this->set('all', $user);
            $this->set('user', $user['User']);
            $this->set('isAjax', $this->RequestHandler->isAjax());
            #$pokes = array_merge($user['PokeTo'], $user['PokeFrom']);
            #rsort($pokes);
            #$this->set('pokes', $pokes);
            // FIXME Inefficient
            $this->set('PokeToCount', count($this->Poke->findAll(array('Poke.to_user_id'=>$user['User']['id']))));
            $this->set('PokeFromCount', count($this->Poke->findAll(array('Poke.from_user_id'=>$user['User']['id']))));
            $this->set('PokeTo', $this->paginate('Poke', array('Poke.to_user_id'=>$user['User']['id'])));
            $this->set('PokeFrom', $this->paginate('Poke', array('Poke.from_user_id'=>$user['User']['id'])));
        }
        else {
            $this->set('user', false);
        }
    }
    
    function poke() {
        $p = $this->params['pass'];
        $user = $this->User->find('first', array('conditions' => array('User.username' => $p[0])));
        $u = $this->Auth->user();
        $u1 = $u['User']['id'];
        $u2 = $user['User']['id'];
        if ($u1 == $u2) {
            $this->flash('Sorry, you cannot poke yourself.', '/'.$p[0]);
        }
        else {
            if ($u1 && $u2) {
                $poke = array(
                        'Poke'=>array('from_user_id'=>$u1, 'to_user_id'=>$u2)
                    );
                $this->Poke->save($poke);
                $this->redirect('/'.$p[0]);
            }
            else {
                $this->flash('Poke failed', '/'.$p[0]);
            }
        }
    }
    
    function beforeFilter() {
        $this->Auth->allow('*');
        $this->RequestHandler->setContent('json', 'text/x-json');
    }
}

?>