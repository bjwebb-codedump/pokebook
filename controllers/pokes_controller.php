<?php
class PokesController extends AppController
{
    var $name = 'Pokes';
    var $components = array('RequestHandler');
    var $helpers = array('Form', 'Html', 'Javascript');
    var $paginate = array(
        'limit' => 40
    );

    
    function index() {
        $this->Poke->recursive = 0;
        $pokes = $this->paginate('Poke');
        $this->set('pokes', $pokes);
    }
    
    function view() {
        $p = $this->params['pass'];
        if (count($p)) {
            $poke = $this->Poke->find('first', array('conditions' => array('Poke.id' => $p[0])));
            $this->set('poke', $poke);
        }
        else {
            $this->set('poke', false);
        }
    }
    
    function beforeFilter() {
        $this->RequestHandler->setContent('json', 'text/x-json');
    }
}
?>